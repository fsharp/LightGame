#I "../node_modules/"
#I "../node_modules/fable-arch/"     // for fs files loaded below   
#r "fable-core/Fable.Core.dll"

#load "Fable.Arch.Html.fs"
#load "Fable.Arch.App.fs"
#load "Fable.Arch.Virtualdom.fs"
// http://kcieslak.io/Getting-Started-with-Fable-and-Webpack

open Fable.Core 
open Fable.Core.JsInterop

open Fable.Arch
open Fable.Arch.App
open Fable.Arch.Html

type Model = bool list

type Actions = Toggle of int

let update model = function
    | Toggle indexOfButton -> 
        model
        |> List.mapi (fun i x -> if abs(i - indexOfButton) < 2 then not x else x)

let lightButton indexOfButton valueOfButton =
    let color = if valueOfButton then " on" else ""
    div [classy ("lightbutton" + color); onMouseClick (fun e -> Toggle indexOfButton)][]

let view model =
    model
    |> List.mapi lightButton
    |> div []

let simpleAppCreator update view model = createSimpleApp model view update Virtualdom.createRender

[true; false; true; false; true; false; true]
|> simpleAppCreator update view
|> withStartNodeSelector "#hello"
|> start
